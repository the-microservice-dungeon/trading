# helm-trading

helm chart for trading

run following a folder above

```
helm install trading helm-chart --namespace trading --create-namespace
```

OR via registry
```
helm install trading dungeon/trading --namespace trading --create-namespace
```
service will be exposed at port `30005` after `minikube tunnel`
