[![pipeline status](https://gitlab.com/the-microservice-dungeon/trading/badges/main/pipeline.svg)](https://gitlab.com/the-microservice-dungeon/trading/-/commits/main) | 
[![coverage report](https://gitlab.com/the-microservice-dungeon/trading/badges/main/coverage.svg)](https://gitlab.com/the-microservice-dungeon/trading/-/commits/main) | 
[Test Coverage Report](https://gitlab.com/the-microservice-dungeon/trading/-/jobs/artifacts/main/file/target/site/jacoco/index.html?job=test:jdk17) | 
[Code Quality Report](https://the-microservice-dungeon.gitlab.io/trading)

___

# trading

## Running using Local Dev Env

The easiest way to start development is by using the [local-dev-env](https://gitlab.com/the-microservice-dungeon/local-dev-environment).
Which provides you with a full environment to develop the service.
However, the local-dev-environment will also start a service by itself which blocks the port.
Therefore, make sure to don't start the game service by following the instructions in the local-dev-env
documentation.

### Setup for local testing
1. install a local apache client like laragon or xampp
2. create a local mysql database for the project
3. configure data in application.properties or set system environment variables

### Tests
Most things in this service have tests.
The tests ending with "IntegrationTest" will fail locally.
These should only be able to run correctly with the other services deployed.

### Rest Api:
https://the-microservice-dungeon.github.io/docs/openapi/trading
### Event Api:
https://the-microservice-dungeon.github.io/docs/asyncapi/trading
