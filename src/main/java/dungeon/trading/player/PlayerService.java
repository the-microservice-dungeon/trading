package dungeon.trading.player;

import dungeon.trading.player.dto.PlayerStatusDto;
import java.util.UUID;

public interface PlayerService {
    void createOrUpdate(PlayerStatusDto playerStatusDto);
    Player findPlayerByID(UUID playerId);
}
