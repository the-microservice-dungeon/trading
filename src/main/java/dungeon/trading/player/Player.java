package dungeon.trading.player;

import dungeon.trading.player.events.PlayerCreated;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.springframework.data.domain.AbstractAggregateRoot;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Player extends AbstractAggregateRoot<Player> {
    @Id
    @Type(type = "uuid-char")
    private UUID id;

    @Setter
    private String name;

    public Player(UUID id, String name) {
        this.id = id;
        this.name = name;
        this.registerEvent(new PlayerCreated(id, name));
    }
}
