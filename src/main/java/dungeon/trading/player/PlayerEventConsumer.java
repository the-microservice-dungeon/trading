package dungeon.trading.player;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dungeon.trading.player.dto.PlayerStatusDto;
import dungeon.trading.player.events.PlayerRegistered;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PlayerEventConsumer {
    private final PlayerService playerService;

    private final ObjectMapper objectMapper;
    private final ApplicationEventPublisher applicationEventPublisher;

    public PlayerEventConsumer(PlayerService playerService,
        ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher) {
        this.playerService = playerService;
        this.objectMapper = objectMapper;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @KafkaListener(topics = "playerStatus", groupId = "trading", autoStartup = "true")
    public void listenToPlayerCreation(ConsumerRecord<String, String> consumerRecord) {
        try {
            var player = this.objectMapper.readValue(consumerRecord.value(), PlayerStatusDto.class);
            this.playerService.createOrUpdate(player);
            applicationEventPublisher.publishEvent(new PlayerRegistered(player.playerId(), player.gameId()));
        } catch (JsonProcessingException e) {
            log.error("Could not process playerStatus event", e);
        }
    }
}
