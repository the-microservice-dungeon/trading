package dungeon.trading.player.events;

import java.util.UUID;

public record PlayerRegistered(
    UUID id,
    UUID gameId
) {

}
