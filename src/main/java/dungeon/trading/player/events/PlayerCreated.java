package dungeon.trading.player.events;

import java.util.UUID;

public record PlayerCreated(
    UUID id,
    String name
) {

}
