package dungeon.trading.player;

import dungeon.trading.player.dto.PlayerStatusDto;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class PlayerServiceImpl implements PlayerService {

    private final PlayerRepository playerRepository;

    public PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Transactional
    public void createOrUpdate(PlayerStatusDto playerStatusDto) {
        var playerId = playerStatusDto.playerId();
        var player = this.playerRepository.findById(playerId);
        Player newPlayer;
        if(player.isPresent()) {
            newPlayer = player.get();
            newPlayer.setName(playerStatusDto.name());
        } else {
            newPlayer = new Player(playerId, playerStatusDto.name());
        }
        this.playerRepository.save(newPlayer);
    }

    public Player findPlayerByID(UUID playerId) {
        return this.playerRepository.findById(playerId).orElseThrow();
    }
}
