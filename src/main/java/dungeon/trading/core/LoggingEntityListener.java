package dungeon.trading.core;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LoggingEntityListener {
    @PrePersist
    public void prePersist(Object entity) {
        log.trace("Persisting entity {}", entity);
    }

    @PreUpdate
    public void preUpdate(Object entity) {
        log.trace("Updating entity {}", entity);
    }
}
