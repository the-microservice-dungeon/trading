package dungeon.trading.command;

import dungeon.trading.command.dto.CommandDto;
import dungeon.trading.tradables.TradableFacade;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class CommandController {

    private final TradableFacade tradableFacade;

    public CommandController(TradableFacade tradableFacade) {
        this.tradableFacade = tradableFacade;
    }

    /**
     * main post controller for commands that have to be handled in trading
     *
     * @param commands requestbody of json-string-array with commands
     * @return 200 Ok
     */
    @PostMapping("/commands")
    public ResponseEntity<?> processInComingTradingCommands(
        @RequestBody List<CommandDto> commands) {
        for (CommandDto command : commands) {
            var commandType = command.payload().commandType();

            try {
                switch (commandType) {
                    case BUY -> {
                        var item = command.payload().itemName();
                        var amount = command.payload().amount();
                        var robotId = command.payload().robotId();
                        var playerId = command.playerId();
                        if(robotId == null) {
                            tradableFacade.buyForPlayer(playerId, item, amount);
                        } else {
                            tradableFacade.buyForRobot(robotId, item, amount);
                        }
                    }
                    case SELL -> {
                        this.tradableFacade.sellInventory(command.payload().robotId());
                    }
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
