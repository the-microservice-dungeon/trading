package dungeon.trading.bank.event;

import java.math.BigDecimal;
import java.util.UUID;

public record BankAccountWithdrawalRequested(
    UUID playerId,
    BigDecimal withdrawalAmount
) {

}
