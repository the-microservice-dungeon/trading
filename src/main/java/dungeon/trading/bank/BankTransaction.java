package dungeon.trading.bank;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class BankTransaction {
    @NotNull
    private LocalDateTime timestamp;
    @NotNull
    private BigDecimal amount;

    public BankTransaction(@NotNull LocalDateTime timestamp, @NotNull BigDecimal amount) {
        if(amount.equals(BigDecimal.ZERO))
            throw new IllegalArgumentException("The amount of money in a transaction not be zero.");
        this.timestamp = timestamp;
        this.amount = amount;
    }
}
