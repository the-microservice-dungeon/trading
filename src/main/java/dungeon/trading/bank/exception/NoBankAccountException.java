package dungeon.trading.bank.exception;

public class NoBankAccountException extends RuntimeException {
    public NoBankAccountException(String message) {
        super(message);
    }
}
