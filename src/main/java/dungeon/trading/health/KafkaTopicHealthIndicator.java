package dungeon.trading.health;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.actuate.health.Status;
import org.springframework.data.util.Pair;
import org.springframework.kafka.core.KafkaAdminOperations;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A custom health indicator checking the availability of map topics.
 *
 * @author Daniel Köllgen
 * @since 09.11.2024
 */
@Component
public class KafkaTopicHealthIndicator implements HealthIndicator {

    private final KafkaAdminOperations admin;

    @Autowired
    public KafkaTopicHealthIndicator(KafkaAdminOperations admin) {
        this.admin = admin;
    }

    /**
     * Verifies, that the given topics have been created. Reports healthy if all topics exist.
     *
     * @return an aggregated health status with details for each topic.
     */
    @Override
    public Health health() {
        List<String> topics = List.of("bank", "prices", "trade-sell", "trade-buy");

        Map<String, Status> details = topics.stream().map(topic -> {
            try {
                admin.describeTopics(topic);
                return Pair.of(topic, Status.UP);
            } catch (Exception e) {
                return Pair.of(topic, Status.DOWN);
            }
        }).collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));

        Status aggStatus = details.values().stream().reduce(Status.UP, (a, b) -> {
            if (a.equals(Status.UP) && b.equals(Status.UP)) {
                return Status.UP;
            }
            return Status.DOWN;
        });

        return Health.status(aggStatus).withDetails(details).build();
    }
}
