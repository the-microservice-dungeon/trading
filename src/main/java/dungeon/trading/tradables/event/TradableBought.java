package dungeon.trading.tradables.event;

import dungeon.trading.tradables.TradableType;
import java.math.BigDecimal;
import java.util.UUID;

public record TradableBought(
    UUID playerId,
    UUID robotId,
    TradableType type,
    String name,
    Integer amount,
    BigDecimal pricePerUnit,
    BigDecimal totalPrice
) {
}
