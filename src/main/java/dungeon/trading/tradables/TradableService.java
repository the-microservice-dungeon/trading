package dungeon.trading.tradables;

import io.micrometer.core.lang.Nullable;
import java.util.UUID;

public interface TradableService {

    void buy(UUID playerId, @Nullable UUID robotId, String name, Integer amount);

    void sell(UUID playerId, UUID robotId, String name, Integer amount);

}
