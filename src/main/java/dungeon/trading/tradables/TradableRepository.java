package dungeon.trading.tradables;

import io.micrometer.core.lang.Nullable;
import java.util.List;

public interface TradableRepository {
    @Nullable Tradable findByName(String name);
    List<Tradable> findByType(TradableType type);
    List<Tradable> findAll();
}
