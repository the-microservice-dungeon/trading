package dungeon.trading.tradables;

public enum TradableType {
    ITEM,
    RESTORATION,
    UPGRADE,
    RESOURCE
}
