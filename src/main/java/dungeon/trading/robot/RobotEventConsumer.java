package dungeon.trading.robot;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dungeon.trading.robot.dto.incoming.RobotResourceMinedDto;
import dungeon.trading.robot.dto.incoming.RobotAttackedDto;
import dungeon.trading.robot.dto.incoming.RobotSpawnedDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RobotEventConsumer {

    private final RobotService robotService;

    private final ObjectMapper objectMapper;

    public RobotEventConsumer(
        RobotService robotService, ObjectMapper objectMapper) {
        this.robotService = robotService;
        this.objectMapper = objectMapper;
    }

    @KafkaListener(topics = "robot", groupId = "trading", autoStartup = "true")
    public void listenToRobotEvents(@Header("type") String type, ConsumerRecord<String, String> record) {
        switch (type) {
            case "RobotSpawned" -> handleRobotSpawned(record);
            case "RobotAttacked" -> handleRobotAttacked(record);
            case "RobotResourceMined" -> handleInventoryUpdated(record);
            default -> log.warn("Unknown type: {}", type);
        }
    }

    private void handleRobotSpawned(ConsumerRecord<String, String> record) {
        try {
            var event = objectMapper.readValue(record.value(), RobotSpawnedDto.class);
            robotService.create(event);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleRobotAttacked(ConsumerRecord<String, String> record) {
        try {
            var event = objectMapper.readValue(record.value(), RobotAttackedDto.class);
            if(!event.attacker().alive()) {
                robotService.delete(event.attacker().robotId());
            }
            if(!event.target().alive()) {
                robotService.delete(event.target().robotId());
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private void handleInventoryUpdated(ConsumerRecord<String, String> record) {
        try {
            var event = objectMapper.readValue(record.value(), RobotResourceMinedDto.class);
            robotService.updateInventory(event);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
