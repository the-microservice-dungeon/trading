package dungeon.trading.robot.events;

import java.util.UUID;

public record RobotInventoryCleared(
    UUID robotId
) {

}
