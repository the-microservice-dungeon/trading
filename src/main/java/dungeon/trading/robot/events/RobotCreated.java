package dungeon.trading.robot.events;

import java.util.UUID;

public record RobotCreated(
    UUID robotId,
    UUID playerId
) {

}
