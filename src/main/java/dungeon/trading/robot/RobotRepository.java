package dungeon.trading.robot;

import java.util.UUID;
import org.springframework.data.repository.CrudRepository;

public interface RobotRepository extends CrudRepository<Robot, UUID> {
}
