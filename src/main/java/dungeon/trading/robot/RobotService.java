package dungeon.trading.robot;

import dungeon.trading.robot.dto.incoming.RobotResourceMinedDto;
import dungeon.trading.robot.dto.incoming.RobotSpawnedDto;
import java.util.UUID;

public interface RobotService {
    void create(RobotSpawnedDto robotSpawned);
    void delete(UUID robotId);
    Robot findById(UUID robotId);
    void updateInventory(RobotResourceMinedDto robotInventoryUpdated);
    void save(Robot robot);
}
