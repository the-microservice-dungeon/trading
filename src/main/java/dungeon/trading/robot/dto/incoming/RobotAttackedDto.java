package dungeon.trading.robot.dto.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotAttackedDto(
    RobotFightResult attacker,
    RobotFightResult target
) {
    @JsonIgnoreProperties(ignoreUnknown = true)
    public record RobotFightResult(
        UUID robotId,
        boolean alive
    ) {}
}
