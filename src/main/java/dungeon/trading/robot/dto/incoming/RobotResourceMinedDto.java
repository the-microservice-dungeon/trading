package dungeon.trading.robot.dto.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotResourceMinedDto(
    UUID robotId,
    RobotResourcesDto resourceInventory
) {
}
