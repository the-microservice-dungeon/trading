package dungeon.trading.robot.dto.incoming;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public record RobotResourcesDto(
    int coal,
    int iron,
    int gem,
    int gold,
    int platin
) {

}
