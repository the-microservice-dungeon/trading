package dungeon.trading.robot;

import dungeon.trading.core.LoggingEntityListener;
import dungeon.trading.player.Player;
import dungeon.trading.robot.events.RobotCreated;
import dungeon.trading.robot.events.RobotInventoryCleared;
import dungeon.trading.robot.events.RobotInventoryUpdated;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Type;
import org.springframework.data.domain.AbstractAggregateRoot;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
@EntityListeners(LoggingEntityListener.class)
public class Robot extends AbstractAggregateRoot<Robot> {
    @Id
    @Type(type = "uuid-char")
    private UUID id;

    @ManyToOne(optional = false)
    private Player player;

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, Integer> inventory = new HashMap<>();

    public Robot(UUID id, Player player) {
        this.id = id;
        this.player = player;
        this.registerEvent(new RobotCreated(id, player.getId()));
    }

    public Map<String, Integer> getInventory() {
        return Map.copyOf(inventory);
    }

    public void clearInventory() {
        inventory.clear();
        this.registerEvent(new RobotInventoryCleared(id));
    }

    public void setInventory(Map<String, Integer> inventory) {
        this.inventory = inventory;
        this.registerEvent(new RobotInventoryUpdated(id, inventory));
    }
}
