package dungeon.trading.robot;

import dungeon.trading.player.PlayerService;
import dungeon.trading.robot.dto.incoming.RobotResourceMinedDto;
import dungeon.trading.robot.dto.incoming.RobotSpawnedDto;
import java.util.Map;
import java.util.UUID;
import org.springframework.stereotype.Service;

@Service
public class RobotServiceImpl implements RobotService {
    private final RobotRepository robotRepository;
    private final PlayerService playerService;

    public RobotServiceImpl(RobotRepository robotRepository, PlayerService playerService) {
        this.robotRepository = robotRepository;
        this.playerService = playerService;
    }

    public void create(RobotSpawnedDto robotSpawned) {
        var robotDto = robotSpawned.robot();
        var player = playerService.findPlayerByID(robotDto.player());
        var robot = new Robot(robotDto.id(), player);
        this.robotRepository.save(robot);
    }

    @Override
    public void delete(UUID robotId) {
        this.robotRepository.deleteById(robotId);
    }

    @Override
    public void updateInventory(RobotResourceMinedDto robotInventoryUpdated) {
        var robot = this.robotRepository.findById(robotInventoryUpdated.robotId()).orElseThrow();
        var resources = robotInventoryUpdated.resourceInventory();
        var inventory = Map.of(
            "COAL", resources.coal(),
            "IRON", resources.iron(),
            "GEM", resources.gem(),
            "GOLD", resources.gold(),
            "PLATIN", resources.platin()
        );
        robot.setInventory(inventory);
        this.robotRepository.save(robot);
    }

    @Override
    public Robot findById(UUID robotId) {
        return robotRepository.findById(robotId).orElseThrow();
    }

    @Override
    public void save(Robot robot) {
        this.robotRepository.save(robot);
    }
}
