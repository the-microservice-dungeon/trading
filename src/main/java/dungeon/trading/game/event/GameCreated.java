package dungeon.trading.game.event;

import java.util.UUID;

public record GameCreated(
    UUID id
) {
}
