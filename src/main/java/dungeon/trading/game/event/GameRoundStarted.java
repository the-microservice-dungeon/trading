package dungeon.trading.game.event;

import java.util.UUID;

public record GameRoundStarted(
    UUID id,
    int round
) {
}
