package dungeon.trading.game.event;

import java.util.List;
import java.util.UUID;

public record GameStarted(
    UUID id,
    List<UUID> participatingPlayers
) {
}
