package dungeon.trading.game.event;

import java.util.UUID;

public record GameStopped(
    UUID id
) {
}
