package dungeon.trading.game;

public enum GameStatus {
    ENDED,
    CREATED,
    STARTED
}
