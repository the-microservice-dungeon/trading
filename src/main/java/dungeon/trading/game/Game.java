package dungeon.trading.game;

import dungeon.trading.game.event.GameCreated;
import dungeon.trading.game.event.GameRoundStarted;
import dungeon.trading.game.event.GameStarted;
import dungeon.trading.game.event.GameStopped;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.springframework.data.domain.AbstractAggregateRoot;

/**
 * Game manages the round count and status for our service
 * needed for the calculation of item and resource prices
 * needed for the saving of item and resource histories
 */
@Entity
@Getter
@NoArgsConstructor
public class Game extends AbstractAggregateRoot<Game> {
    @Id
    @Type(type = "uuid-char")
    private UUID gameId;

    private Boolean isCurrentGame;

    private int currentRound;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<UUID> registeredPlayers = new ArrayList<>();


    public Game(UUID newGame) {
        this.gameId = newGame;
        this.currentRound = 0;
        this.isCurrentGame = false;
        this.registerEvent(new GameCreated(newGame));
    }

    public void setRound(int round) {
        this.currentRound = round;
        this.registerEvent(new GameRoundStarted(this.gameId, this.currentRound));
    }

    public void stopGame() {
        this.isCurrentGame = false;
        this.registerEvent(new GameStopped(this.gameId));
    }

    public void startGame() {
        this.isCurrentGame = true;
        this.registerEvent(new GameStarted(this.gameId, this.registeredPlayers));
    }

    public void registerPlayer(UUID id) {
        this.registeredPlayers.add(id);
    }

    public List<UUID> getRegisteredPlayers() {
        return List.copyOf(this.registeredPlayers);
    }
}
