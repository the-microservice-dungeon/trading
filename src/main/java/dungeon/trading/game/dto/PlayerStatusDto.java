package dungeon.trading.game.dto;

import dungeon.trading.game.GameStatus;
import java.util.UUID;

public record PlayerStatusDto(
    UUID gameId,
    UUID playerId
) {}
