package dungeon.trading.game;

import dungeon.trading.game.dto.RoundDto;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class GameServiceImpl implements GameService {

    private final GameRepository gameRepository;


    public GameServiceImpl(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public void create(UUID id) {
        var game = new Game(id);
        this.save(game);
    }

    @Override
    public Game findById(UUID id) {
        return this.gameRepository.findById(id).orElseThrow();
    }

    @Override
    public void start(UUID gameId) {
        this.gameRepository.findByIsCurrentGame(true)
            .ifPresent(game -> stop(game.getGameId()));
        var game = this.findById(gameId);
        game.startGame();
        this.save(game);
    }

    @Override
    public void stop(UUID gameId) {
        var game = this.findById(gameId);
        game.stopGame();
        this.save(game);
    }

    @Transactional
    public void updateRound(RoundDto roundDto) {
        Optional<Game> newGame = this.gameRepository.findById(roundDto.gameId());
        var game = newGame.orElseThrow();
        game.setRound(roundDto.roundNumber());
        this.save(game);
    }

    @Override
    public void save(Game game) {
        this.gameRepository.save(game);
    }

    @Override
    public void registerPlayer(UUID gameId, UUID playerId) {
        var game = this.gameRepository.findById(gameId).orElseThrow();
        game.registerPlayer(playerId);
        this.gameRepository.save(game);
    }
}

