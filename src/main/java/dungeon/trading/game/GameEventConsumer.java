package dungeon.trading.game;

import com.fasterxml.jackson.databind.ObjectMapper;
import dungeon.trading.game.dto.GameStatusDto;
import dungeon.trading.game.dto.PlayerStatusDto;
import dungeon.trading.game.dto.RoundDto;
import dungeon.trading.player.events.PlayerRegistered;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GameEventConsumer {

    private final GameService gameService;

    private final ObjectMapper objectMapper;

    public GameEventConsumer(GameService gameService,
        ObjectMapper objectMapper) {
        this.gameService = gameService;
        this.objectMapper = objectMapper;
    }

    @KafkaListener(topics = "status", groupId = "trading", autoStartup = "true")
    public void listenToGameStatus(ConsumerRecord<String, String> consumerRecord) {
        try {
            GameStatusDto statusDto = this.objectMapper.readValue(consumerRecord.value(),
                GameStatusDto.class);

            if (statusDto.status() == GameStatus.CREATED) {
                this.gameService.create(statusDto.gameId());
            } else if (statusDto.status() == GameStatus.ENDED) {
                this.gameService.stop(statusDto.gameId());
            } else if (statusDto.status() == GameStatus.STARTED) {
                this.gameService.start(statusDto.gameId());
            }
        } catch (Exception e) {
            log.error("Error during 'status' event consumption", e);
        }
    }

    @KafkaListener(topics = "roundStatus", groupId = "trading", autoStartup = "true")
    public void listenToRoundStarted(ConsumerRecord<String, String> consumerRecord) {
        try {
            RoundDto round = this.objectMapper.readValue(consumerRecord.value(), RoundDto.class);

            this.gameService.updateRound(round);
        } catch (Exception e) {
            log.error("Error during 'roundStatus' event consumption", e);
        }
    }

    @EventListener(PlayerRegistered.class)
    public void listenToPlayerStatus(PlayerRegistered playerRegistered) {
        this.gameService.registerPlayer(playerRegistered.gameId(), playerRegistered.id());
    }
}
