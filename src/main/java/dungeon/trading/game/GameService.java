package dungeon.trading.game;

import dungeon.trading.game.dto.RoundDto;
import java.util.UUID;

public interface GameService
{
    void create(UUID id);
    Game findById(UUID id);
    void start(UUID gameId);
    void stop(UUID gameId);
    void updateRound(RoundDto roundDto);
    void save(Game game);
    void registerPlayer(UUID gameId, UUID playerId);
}

