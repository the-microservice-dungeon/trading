package dungeon.trading.player;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

import dungeon.trading.bank.BankAccountRepository;
import dungeon.trading.game.Game;
import dungeon.trading.game.GameRepository;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.transaction.Transactional;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

// It can happen that the template sends records to the broker before the consumer has subscribed
// to the topic. To overcome this, we simply adjust the offset-reset to earliest (default: latest)
@SpringBootTest(properties = {"spring.kafka.consumer.auto-offset-reset=earliest"})
@DirtiesContext
@EmbeddedKafka(topics = { "playerStatus", "status" }, bootstrapServersProperty = "spring.kafka.bootstrap-servers")
class PlayerEventConsumerIntegrationTest {

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    GameRepository gameRepository;
    @Autowired
    BankAccountRepository bankAccountRepository;

    @Test
    void shouldCreatePlayerOnPlayerStatusEvent() {
        var playerId = UUID.randomUUID();
        var message = """
            {
              "playerId": "%s",
              "name": "John Doe"
            }
            """.formatted(playerId);
        var record = new ProducerRecord<>("playerStatus", playerId.toString(), message);
        kafkaTemplate.send(record).addCallback(it -> {}, it -> { throw new RuntimeException(it); });

        var player = await().atMost(5, TimeUnit.SECONDS)
            .until(() -> playerRepository.findById(playerId), Optional::isPresent)
            .get();
        assertThat(player)
            .extracting(Player::getId, Player::getName)
            .containsExactly(playerId, "John Doe");
    }

    @Test
    void shouldInitializeBankAccountOnGameStartedEvent() {
        var playerId = UUID.randomUUID();
        var gameId = UUID.randomUUID();
        var game = new Game(gameId);
        game.registerPlayer(playerId);
        playerRepository.save(new Player(playerId, "John Doe"));
        gameRepository.save(game);
        var message = """
            {
              "gameId": "%s",
              "status": "started"
            }
            """.formatted(gameId);
        var record = new ProducerRecord<>("status", gameId.toString(), message);
        kafkaTemplate.send(record).addCallback(it -> {}, it -> { throw new RuntimeException(it); });

        var player = await().atMost(5, TimeUnit.SECONDS)
            .until(() -> bankAccountRepository.findBankAccountByPlayerId(playerId), Optional::isPresent)
            .get();
        assertThat(player.getTransactions())
            .hasSize(1);
        assertThat(player.getBalance())
            .isEqualByComparingTo(BigDecimal.valueOf(500));
    }
}
