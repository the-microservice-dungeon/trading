package dungeon.trading.player;

import static org.assertj.core.api.Assertions.assertThat;

import dungeon.trading.player.dto.PlayerStatusDto;
import dungeon.trading.player.events.PlayerCreated;
import java.util.Optional;
import java.util.UUID;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;


@SpringBootTest
@RecordApplicationEvents
@AutoConfigureTestDatabase
class PlayerServiceIntegrationTest {
    @Autowired
    private ApplicationEvents applicationEvents;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayerRepository playerRepository;

    @Test
    @Transactional
    void playerCreationTest() {
        var playerId = UUID.randomUUID();
        var dto = new PlayerStatusDto(playerId, UUID.randomUUID(), "TestPlayer");
        this.playerService.createOrUpdate(dto);
        Optional<Player> player = this.playerRepository.findById(playerId);
        assertThat(player)
            .isPresent()
            .get()
            .extracting(Player::getId)
            .isEqualTo(playerId);
    }

    @Test
    void shouldPublishPlayerRegistered() {
        var player = new PlayerStatusDto(UUID.randomUUID(), UUID.randomUUID(), "John Doe");
        playerService.createOrUpdate(player);

        assertThat(applicationEvents.stream(PlayerCreated.class))
            .hasSize(1)
            .first()
            .extracting(PlayerCreated::id, PlayerCreated::name)
            .containsExactly(player.playerId(), "John Doe");
    }
}
