package dungeon.trading.game;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

import dungeon.trading.bank.BankAccount;
import dungeon.trading.bank.BankAccountRepository;
import dungeon.trading.player.Player;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.util.Streamable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest(properties = {"spring.kafka.consumer.auto-offset-reset=earliest"})
@DirtiesContext
@EmbeddedKafka(topics = {"status"}, bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@Transactional
class GameEventConsumerIntegrationTest {

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    EntityManager entityManager;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Test
    void shouldResetAllBankAccountsWhenGameEnds() {
        var bankAccount1 = randomBankAccount();
        var bankAccount2 = randomBankAccount();
        bankAccount1.deposit(BigDecimal.valueOf(100));
        bankAccount1.withdraw(BigDecimal.valueOf(10));

        bankAccount2.deposit(BigDecimal.valueOf(2000));
        bankAccount2.withdraw(BigDecimal.valueOf(100));

        entityManager.persist(bankAccount1);
        entityManager.persist(bankAccount2);

        var game = new Game(UUID.fromString("5bc9f935-32f1-4d7b-a90c-ff0e6e34125a"));
        game.startGame();
        entityManager.persist(game);

        assertThat(bankAccountRepository.findAll())
            .hasSizeGreaterThan(0);

        var message = """
            {
              "gameId": "%s",
              "status": "ended"
            }
            """.formatted(game.getGameId());
        var record = new ProducerRecord<>("status", game.getGameId().toString(),
            message);
        kafkaTemplate.send(record).addCallback(it -> {
        }, it -> {
            throw new RuntimeException(it);
        });

        await().atMost(5, TimeUnit.SECONDS)
            .until(() -> Streamable.of(bankAccountRepository.findAll()).stream().allMatch(this::isReset));
    }

    private boolean isReset(BankAccount bankAccount) {
        return bankAccount.getBalance().compareTo(BigDecimal.ZERO) == 0 && bankAccount.getTransactions().isEmpty();
    }

    private BankAccount randomBankAccount() {
        var player = new Player(UUID.randomUUID(), "John Doe");
        entityManager.persist(player);
        var balance = BigDecimal.valueOf(Math.random() * 100);
        return bankAccountRepository.save(new BankAccount(player, balance));
    }
}
