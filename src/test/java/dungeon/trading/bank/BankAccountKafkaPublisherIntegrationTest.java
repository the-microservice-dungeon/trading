package dungeon.trading.bank;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dungeon.trading.bank.event.BankAccountCleared;
import dungeon.trading.bank.event.BankAccountInitialized;
import dungeon.trading.bank.event.BankAccountTransactionBooked;
import java.math.BigDecimal;
import java.util.UUID;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import javax.transaction.Transactional;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

@SpringBootTest(properties = {"spring.kafka.consumer.auto-offset-reset=earliest"})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@EmbeddedKafka(topics = {"bank"}, bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@Transactional
class BankAccountKafkaPublisherIntegrationTest {

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    ObjectMapper objectMapper;

    BlockingDeque<ConsumerRecord<String, String>> records = new LinkedBlockingDeque<>();

    @KafkaListener(topics = "bank")
    void listen(ConsumerRecord<String, String> message) {
        records.add(message);
    }

    @Test
    void shouldPublishBankInitializedEventToKafka()
        throws InterruptedException, JsonProcessingException {
        var bankAccountInitialized = new BankAccountInitialized(UUID.randomUUID(), BigDecimal.valueOf(100));
        applicationEventPublisher.publishEvent(bankAccountInitialized);

        var record = records.pollFirst(5, TimeUnit.SECONDS);
        assertThat(new String(record.headers().lastHeader("type").value()))
            .isEqualTo("BankAccountInitialized");
        assertThat(new String(record.headers().lastHeader("transactionId").value()))
            .isEqualTo(bankAccountInitialized.key());
        assertThat(record.headers().lastHeader("version"))
            .isNotNull();
        assertThat(record.headers().lastHeader("timestamp"))
            .isNotNull();
        assertThat(record.headers().lastHeader("eventId"))
            .isNotNull();
        assertThat(new String(record.headers().lastHeader("playerId").value()))
            .isEqualTo(bankAccountInitialized.playerId().toString());

        var bankAccount = objectMapper.readValue(record.value(), BankAccountInitialized.class);
        assertThat(bankAccount)
            .isEqualTo(bankAccountInitialized);
    }

    @Test
    void shouldPublishBankAccountClearedEventToKafka()
        throws InterruptedException, JsonProcessingException {
        var bankAccountCleared = new BankAccountCleared(UUID.randomUUID(), BigDecimal.valueOf(100));
        applicationEventPublisher.publishEvent(bankAccountCleared);

        var record = records.pollFirst(5, TimeUnit.SECONDS);
        assertThat(new String(record.headers().lastHeader("type").value()))
            .isEqualTo("BankAccountCleared");
        assertThat(new String(record.headers().lastHeader("transactionId").value()))
            .isEqualTo(bankAccountCleared.key());
        assertThat(record.headers().lastHeader("version"))
            .isNotNull();
        assertThat(record.headers().lastHeader("timestamp"))
            .isNotNull();
        assertThat(record.headers().lastHeader("eventId"))
            .isNotNull();
        assertThat(new String(record.headers().lastHeader("playerId").value()))
            .isEqualTo(bankAccountCleared.playerId().toString());

        var bankAccount = objectMapper.readValue(record.value(), BankAccountCleared.class);
        assertThat(bankAccount)
            .isEqualTo(bankAccountCleared);
    }

    @Test
    void shouldPublishBankAccountTransactionBookedEventToKafka()
        throws InterruptedException, JsonProcessingException {
        var bankAccountTransactionBooked = new BankAccountTransactionBooked(UUID.randomUUID(), BigDecimal.valueOf(12), BigDecimal.valueOf(100));
        applicationEventPublisher.publishEvent(bankAccountTransactionBooked);

        var record = records.pollFirst(5, TimeUnit.SECONDS);
        assertThat(new String(record.headers().lastHeader("type").value()))
            .isEqualTo("BankAccountTransactionBooked");
        assertThat(new String(record.headers().lastHeader("transactionId").value()))
            .isEqualTo(bankAccountTransactionBooked.key());
        assertThat(record.headers().lastHeader("version"))
            .isNotNull();
        assertThat(record.headers().lastHeader("timestamp"))
            .isNotNull();
        assertThat(record.headers().lastHeader("eventId"))
            .isNotNull();
        assertThat(new String(record.headers().lastHeader("playerId").value()))
            .isEqualTo(bankAccountTransactionBooked.playerId().toString());

        var bankAccount = objectMapper.readValue(record.value(), BankAccountTransactionBooked.class);
        assertThat(bankAccount)
            .isEqualTo(bankAccountTransactionBooked);
    }


}
