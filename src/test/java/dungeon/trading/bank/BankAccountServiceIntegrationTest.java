package dungeon.trading.bank;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

import dungeon.trading.bank.event.BankAccountCleared;
import dungeon.trading.bank.event.BankAccountInitialized;
import dungeon.trading.bank.event.BankAccountTransactionBooked;
import dungeon.trading.player.Player;
import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;

@SpringBootTest
@RecordApplicationEvents
@AutoConfigureTestDatabase
@Transactional
class BankAccountServiceIntegrationTest {

    @Autowired
    private ApplicationEvents applicationEvents;

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private EntityManager entityManager;

    @BeforeEach
    void cleanup() {
        this.applicationEvents.clear();
    }

    @Test
    void shouldPublishInitializedEvent() {
        var player = new Player(UUID.randomUUID(), "player");
        entityManager.persist(player);
        bankAccountService.initializeForPlayer(player.getId(), BigDecimal.valueOf(200));

        assertThat(applicationEvents.stream(BankAccountInitialized.class))
            .hasSize(1)
            .first()
            .extracting(BankAccountInitialized::playerId, BankAccountInitialized::balance)
            .containsExactly(player.getId(), BigDecimal.valueOf(200));
    }

    @Test
    void shouldPublishTransactionEvent() {
        var bankAccount = bankAccount();
        var player = bankAccount.getPlayer();

        this.bankAccountRepository.save(bankAccount);

        bankAccount.deposit(BigDecimal.valueOf(1000));
        bankAccount.withdraw(BigDecimal.valueOf(200));
        bankAccount.deposit(BigDecimal.valueOf(123));

        this.bankAccountService.save(bankAccount);

        assertThat(applicationEvents.stream(BankAccountTransactionBooked.class))
            .hasSize(3)
            .extracting(BankAccountTransactionBooked::playerId,
                BankAccountTransactionBooked::transactionAmount,
                BankAccountTransactionBooked::balance)
            .containsExactly(
                tuple(player.getId(), BigDecimal.valueOf(1000), BigDecimal.valueOf(1000)),
                tuple(player.getId(), BigDecimal.valueOf(-200), BigDecimal.valueOf(800)),
                tuple(player.getId(), BigDecimal.valueOf(123), BigDecimal.valueOf(923))
            );
    }

    @Test
    void shouldPublishClearedEvent() {
        var bankAccount1 = bankAccount();
        var bankAccount2 = bankAccount();

        bankAccountRepository.save(bankAccount1);
        bankAccountRepository.save(bankAccount2);

        this.bankAccountService.resetAllBankAccounts();

        assertThat(applicationEvents.stream(BankAccountCleared.class))
            .hasSize(2)
            .extracting(BankAccountCleared::playerId, BankAccountCleared::balance)
            .containsExactly(
                tuple(bankAccount1.getPlayer().getId(), BigDecimal.ZERO),
                tuple(bankAccount2.getPlayer().getId(), BigDecimal.ZERO)
            );
    }


    private BankAccount bankAccount() {
        var player = new Player(UUID.randomUUID(), "player");
        entityManager.persist(player);
        return new BankAccount(player);
    }
}
