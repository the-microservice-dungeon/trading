package dungeon.trading.bank;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import dungeon.trading.bank.exception.BalanceNotSufficientException;
import dungeon.trading.player.Player;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class BankAccountTest {
    @Test
    void shouldThrowBalanceNotSufficentException() {
        BankAccount bankAccount = bankAccount(BigDecimal.ZERO);
        assertThrows(BalanceNotSufficientException.class, () -> bankAccount.withdraw(BigDecimal.ONE));
    }

    @Test
    void shouldThrowIllegalArgumentException() {
        BankAccount bankAccount = bankAccount(BigDecimal.ONE);
        assertThrows(IllegalArgumentException.class, () -> bankAccount.withdraw(BigDecimal.valueOf(-1)));
    }

    @Test
    void shouldWithdrawEvenAmount() {
        BankAccount bankAccount = bankAccount(BigDecimal.ONE);
        bankAccount.withdraw(BigDecimal.ONE);
        assertThat(bankAccount.getBalance())
            .isEqualTo(BigDecimal.ZERO);
    }

    @Test
    void shouldWithdrawUnEvenAmount() {
        BankAccount bankAccount = bankAccount(BigDecimal.ONE);
        bankAccount.withdraw(BigDecimal.valueOf(0.2));
        assertThat(bankAccount.getBalance())
            .isEqualTo(BigDecimal.valueOf(0.8));
    }

    @Test
    void shouldDeposit() {
        BankAccount bankAccount = bankAccount(BigDecimal.ONE);
        bankAccount.deposit(BigDecimal.ONE);
        assertThat(bankAccount.getBalance())
            .isEqualTo(BigDecimal.valueOf(2));
    }

    @Test
    void shouldDepositWithoutFloatingPoint() {
        BankAccount bankAccount = bankAccount(BigDecimal.valueOf(0.1));
        bankAccount.deposit(BigDecimal.valueOf(0.2));
        assertThat(bankAccount.getBalance())
            .isEqualTo(BigDecimal.valueOf(0.3));
    }

    @Test
    void shouldReturnImmutableList() {
        BankAccount bankAccount = bankAccount(BigDecimal.ONE);
        bankAccount.deposit(BigDecimal.ONE);
        assertThrows(UnsupportedOperationException.class, () -> bankAccount.getTransactions().add(new BankTransaction(LocalDateTime.now(), BigDecimal.ONE)));
    }

    private BankAccount bankAccount(BigDecimal startBalance) {
        var player = new Player(UUID.randomUUID(), "John Doe");
        return new BankAccount(player, startBalance);
    }
}
