package dungeon.trading.bank;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;

class BankTransactionTest {

    @Test
    void shouldThrowWhenAmountIsZero() {
        assertThrows(IllegalArgumentException.class, () -> new BankTransaction(null, BigDecimal.ZERO));
    }
}
