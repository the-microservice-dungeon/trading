package dungeon.trading.bank;

import static org.assertj.core.api.Assertions.assertThat;

import dungeon.trading.game.Game;
import dungeon.trading.player.Player;
import dungeon.trading.player.events.PlayerRegistered;
import java.math.BigDecimal;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;

@SpringBootTest
@AutoConfigureTestDatabase
@Transactional
class PlayerEventListenerIntegrationTest {

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    EntityManager entityManager;

    @Autowired
    BankAccountService bankAccountService;

    @Test
    void shouldRegisterPlayerOnPlayerRegisteredEvent() {
        var player = new Player(UUID.randomUUID(), "player");
        entityManager.persist(player);
        var game = new Game(UUID.randomUUID());
        entityManager.persist(game);

        this.applicationEventPublisher.publishEvent(new PlayerRegistered(player.getId(), game.getGameId()));

        assertThat(entityManager.find(Game.class, game.getGameId()))
            .isNotNull()
            .satisfies(g -> assertThat(g.getRegisteredPlayers()).contains(player.getId()));
    }

}
