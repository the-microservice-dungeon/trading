package dungeon.trading.robot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

import dungeon.trading.player.Player;
import dungeon.trading.player.PlayerRepository;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;

// It can happen that the template sends records to the broker before the consumer has subscribed
// to the topic. To overcome this, we simply adjust the offset-reset to earliest (default: latest)
@SpringBootTest(properties = {"spring.kafka.consumer.auto-offset-reset=earliest"})
@DirtiesContext
@EmbeddedKafka(topics = { "robot" }, bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@AutoConfigureTestDatabase
class RobotEventConsumerIntegrationTest {

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    RobotRepository robotRepository;

    @Test
    void shouldCreateRobotOnRobotSpawnedEvent() {
        var playerId = UUID.randomUUID();
        var player = new Player(playerId, "John Doe");
        playerRepository.save(player);

        var robotId = UUID.randomUUID();
        var message = """
            {
              "robotId": {
                "id": "%s",
                "alive": true,
                "player": "%s",
                "maxHealth": 100,
                "maxEnergy": 60,
                "energyRegen": 8,
                "attackDamage": 5,
                "miningSpeed": 10,
                "health": 75,
                "energy": 45,
                "healthLevel": 5,
                "damageLevel": 5,
                "miningSpeedLevel": 5,
                "miningLevel": 5,
                "energyLevel": 5,
                "energyRegenLevel": 5,
                "storageLevel": 5
              }
            }
            """.formatted(robotId, playerId);
        var record = new ProducerRecord<>("robot", robotId.toString(), message);
        record.headers().add("type", "RobotSpawned".getBytes(StandardCharsets.UTF_8));
        kafkaTemplate.send(record).addCallback(it -> {}, it -> { throw new RuntimeException(it); });

        var robot = await().atMost(5, TimeUnit.SECONDS)
            .until(() -> robotRepository.findById(robotId), Optional::isPresent)
            .get();
        assertThat(robot)
            .extracting(Robot::getId, r -> r.getPlayer().getId())
            .containsExactly(robotId, playerId);
    }

    @Test
    void shouldDeleteRobotOnRobotKilledEvent() {
        var robot1Id = UUID.randomUUID();
        var robot2Id = UUID.randomUUID();
        var player = new Player(UUID.randomUUID(), "John Doe");
        playerRepository.save(player);
        var robot1 = new Robot(robot1Id, player);
        var robot2 = new Robot(robot2Id, player);
        robotRepository.save(robot1);
        robotRepository.save(robot2);

        assertThat(robotRepository.findById(robot1Id)).isPresent();
        assertThat(robotRepository.findById(robot2Id)).isPresent();

        var message = """
            {
              "attacker": {
                "robotId": "%s",
                "alive": false
              },
              "target": {
                "robotId": "%s",
                "alive": false
              }
            }
            """.formatted(robot1Id, robot2Id);
        var record = new ProducerRecord<>("robot", robot1Id.toString(), message);
        record.headers().add("type", "RobotAttacked".getBytes(StandardCharsets.UTF_8));
        kafkaTemplate.send(record).addCallback(it -> {}, it -> { throw new RuntimeException(it); });

        await().atMost(5, TimeUnit.SECONDS)
            .until(() -> robotRepository.findById(robot1Id), Optional::isEmpty);
        await().atMost(5, TimeUnit.SECONDS)
            .until(() -> robotRepository.findById(robot2Id), Optional::isEmpty);
    }

    @Test
    void shouldUpdateInventory() {
        var robotId = UUID.randomUUID();
        var player = new Player(UUID.randomUUID(), "John Doe");
        playerRepository.save(player);
        var robot = new Robot(robotId, player);
        robotRepository.save(robot);

        assertThat(robotRepository.findById(robotId)).isPresent();

        var message = """
            {
              "robotId": "%s",
              "resourceInventory": {
                "coal": 1,
                "iron": 2,
                "gem": 3,
                "gold": 4,
                "platin": 5
              }
            }
            """.formatted(robotId);
        var record = new ProducerRecord<>("robot", robotId.toString(), message);
        record.headers().add("type", "RobotResourceMined".getBytes(StandardCharsets.UTF_8));
        kafkaTemplate.send(record).addCallback(it -> {}, it -> { throw new RuntimeException(it); });

        Predicate<Robot> inventoryIsUpdated = r -> r.getInventory().get("COAL") == 1
            && r.getInventory().get("IRON") == 2
            && r.getInventory().get("GEM") == 3
            && r.getInventory().get("GOLD") == 4
            && r.getInventory().get("PLATIN") == 5;

        await().atMost(5, TimeUnit.SECONDS)
            .until(() -> robotRepository.findById(robotId).get(), inventoryIsUpdated);
    }
}
