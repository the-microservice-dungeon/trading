package dungeon.trading.robot;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dungeon.trading.tradables.TradableType;
import dungeon.trading.tradables.event.TradableSold;
import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import javax.transaction.Transactional;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.PlatformTransactionManager;

@SpringBootTest(properties = {"spring.kafka.consumer.auto-offset-reset=earliest"})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@EmbeddedKafka(topics = {"trade-sell"}, bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@Transactional
class RobotKafkaPublisherIntegrationTest {

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    PlatformTransactionManager tm;

    BlockingDeque<ConsumerRecord<String, String>> records = new LinkedBlockingDeque<>();

    @KafkaListener(topics = "trade-sell")
    void listen(ConsumerRecord<String, String> message) {
        records.add(message);
    }

    @Test
    void shouldPublishInventorySell()
        throws Exception {
        var robotId = UUID.randomUUID();
        var inventory = Map.of(
            "COAL", 10,
            "IRON", 9,
            "GEM", 8,
            "GOLD", 7
        );

        // Publishes event AFTER the transaction is committed.
        var playerId = UUID.randomUUID();
        applicationEventPublisher.publishEvent(
            new TradableSold(playerId, robotId, TradableType.RESOURCE, "COAL", 10,
                BigDecimal.valueOf(10), BigDecimal.valueOf(100)));
        TestTransaction.flagForCommit();
        TestTransaction.end();

        var record = records.pollFirst(5, TimeUnit.SECONDS);
        assertThat(new String(record.headers().lastHeader("type").value()))
            .isEqualTo("TradableSold");
        assertThat(new String(record.headers().lastHeader("transactionId").value()))
            .isEqualTo(playerId.toString());
        assertThat(record.headers().lastHeader("version").value())
            .isNotNull();
        assertThat(record.headers().lastHeader("timestamp").value())
            .isNotNull();
        assertThat(record.headers().lastHeader("eventId").value())
            .isNotNull();
        assertThat(new String(record.headers().lastHeader("playerId").value()))
            .isEqualTo(playerId.toString());

        var sold = objectMapper.readValue(record.value(), new TypeReference<TradableSold>() {
        });
        assertThat(sold)
            .extracting(TradableSold::playerId, TradableSold::robotId, TradableSold::name,
                TradableSold::type, TradableSold::amount, TradableSold::pricePerUnit,
                TradableSold::totalPrice)
            .containsExactly(playerId, robotId, "COAL", TradableType.RESOURCE, 10,
                BigDecimal.valueOf(10), BigDecimal.valueOf(100));
    }

}
