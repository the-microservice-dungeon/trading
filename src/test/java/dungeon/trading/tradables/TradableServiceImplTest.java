package dungeon.trading.tradables;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import dungeon.trading.bank.event.BankAccountDepositRequested;
import dungeon.trading.bank.event.BankAccountWithdrawalRequested;
import dungeon.trading.tradables.event.TradableBought;
import dungeon.trading.tradables.event.TradableSold;
import dungeon.trading.tradables.exception.TradableNotFoundException;
import java.math.BigDecimal;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.context.ApplicationEventPublisher;

class TradableServiceImplTest {
    TradableRepository tradableRepository = mock(TradableRepository.class);
    ApplicationEventPublisher applicationEventPublisher = mock(ApplicationEventPublisher.class);

    TradableServiceImpl tradableService = new TradableServiceImpl(tradableRepository, applicationEventPublisher);

    @Test
    void shouldThrowWhenBuyAmountIsZero() {
        assertThrows(IllegalArgumentException.class, () -> tradableService.buy(UUID.randomUUID(), UUID.randomUUID(), "name", 0));
    }

    @Test
    void shouldThrowWhenBuyItemIsUnknown() {
        when(tradableRepository.findByName("name")).thenReturn(null);
        assertThrows(
            TradableNotFoundException.class, () -> tradableService.buy(UUID.randomUUID(), UUID.randomUUID(), "name", 1));
    }

    @Test
    void shouldPublishWithdrawalEvent() {
        when(tradableRepository.findByName("name")).thenReturn(new Tradable("name", TradableType.UPGRADE, BigDecimal.ONE));
        ArgumentCaptor<BankAccountWithdrawalRequested> captor = ArgumentCaptor.forClass(
            BankAccountWithdrawalRequested.class);
        var playerId = UUID.randomUUID();

        tradableService.buy(playerId, UUID.randomUUID(), "name", 2);

        verify(applicationEventPublisher, atLeastOnce()).publishEvent(captor.capture());
        assertThat(captor.getAllValues().get(0))
            .extracting(BankAccountWithdrawalRequested::playerId, BankAccountWithdrawalRequested::withdrawalAmount)
            .containsExactly(playerId, BigDecimal.valueOf(2));
    }

    @Test
    void shouldNotPublishWithdrawalEventWhenPriceIsZero() {
        when(tradableRepository.findByName("name")).thenReturn(new Tradable("name", TradableType.UPGRADE, BigDecimal.ZERO));
        var playerId = UUID.randomUUID();

        tradableService.buy(playerId, UUID.randomUUID(), "name", 2);

        verify(applicationEventPublisher, times(0)).publishEvent(isA(BankAccountWithdrawalRequested.class));
    }

    @Test
    void shouldpublishTradableBoughtEvent() {
        when(tradableRepository.findByName("name")).thenReturn(new Tradable("name", TradableType.UPGRADE, BigDecimal.ONE));
        ArgumentCaptor<TradableBought> captor = ArgumentCaptor.forClass(
            TradableBought.class);
        var playerId = UUID.randomUUID();
        var robotId = UUID.randomUUID();

        tradableService.buy(playerId, robotId, "name", 2);

        verify(applicationEventPublisher, atLeastOnce()).publishEvent(captor.capture());
        assertThat(captor.getAllValues().get(1))
            .satisfies(event -> {
                assertThat(event.playerId()).isEqualTo(playerId);
                assertThat(event.robotId()).isEqualTo(robotId);
                assertThat(event.name()).isEqualTo("name");
                assertThat(event.type()).isEqualTo(TradableType.UPGRADE);
                assertThat(event.pricePerUnit()).isEqualTo(BigDecimal.ONE);
                assertThat(event.totalPrice()).isEqualTo(BigDecimal.valueOf(2));
                assertThat(event.amount()).isEqualTo(2);
            });
    }


    @Test
    void shouldThrowWhenSellAmountIsZero() {
        assertThrows(IllegalArgumentException.class, () -> tradableService.sell(UUID.randomUUID(), UUID.randomUUID(), "name", 0));
    }

    @Test
    void shouldThrowWhenSellItemsUnknown() {
        when(tradableRepository.findByName("name")).thenReturn(null);
        assertThrows(TradableNotFoundException.class, () -> tradableService.sell(UUID.randomUUID(), UUID.randomUUID(), "name", 1));
    }

    @Test
    void shouldPublishDepositEvent() {
        when(tradableRepository.findByName("name")).thenReturn(new Tradable("name", TradableType.UPGRADE, BigDecimal.ONE));
        ArgumentCaptor<BankAccountDepositRequested> captor = ArgumentCaptor.forClass(BankAccountDepositRequested.class);
        var playerId = UUID.randomUUID();

        tradableService.sell(playerId, UUID.randomUUID(), "name", 2);

        verify(applicationEventPublisher, atLeastOnce()).publishEvent(captor.capture());
        assertThat(captor.getAllValues().get(0))
            .extracting(BankAccountDepositRequested::playerId, BankAccountDepositRequested::depositAmount)
            .containsExactly(playerId, BigDecimal.valueOf(2));
    }

    @Test
    void shouldNotPublishDepositEventWhenPriceIsZero() {
        when(tradableRepository.findByName("name")).thenReturn(new Tradable("name", TradableType.UPGRADE, BigDecimal.ZERO));
        var playerId = UUID.randomUUID();

        tradableService.sell(playerId, UUID.randomUUID(), "name", 2);

        verify(applicationEventPublisher, times(0)).publishEvent(isA(BankAccountDepositRequested.class));
    }

    @Test
    void shouldpublishTradableSoldEvent() {
        when(tradableRepository.findByName("name")).thenReturn(new Tradable("name", TradableType.UPGRADE, BigDecimal.ONE));
        ArgumentCaptor<TradableSold> captor = ArgumentCaptor.forClass(
            TradableSold.class);
        var playerId = UUID.randomUUID();
        var robotId = UUID.randomUUID();

        tradableService.sell(playerId, robotId, "name", 2);

        verify(applicationEventPublisher, atLeastOnce()).publishEvent(captor.capture());
        assertThat(captor.getAllValues().get(1))
            .satisfies(event -> {
                assertThat(event.playerId()).isEqualTo(playerId);
                assertThat(event.robotId()).isEqualTo(robotId);
                assertThat(event.name()).isEqualTo("name");
                assertThat(event.type()).isEqualTo(TradableType.UPGRADE);
                assertThat(event.pricePerUnit()).isEqualTo(BigDecimal.ONE);
                assertThat(event.totalPrice()).isEqualTo(BigDecimal.valueOf(2));
                assertThat(event.amount()).isEqualTo(2);
            });
    }
}
