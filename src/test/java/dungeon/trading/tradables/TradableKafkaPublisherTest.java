package dungeon.trading.tradables;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dungeon.trading.game.event.GameRoundStarted;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import javax.transaction.Transactional;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

@SpringBootTest(properties = {"spring.kafka.consumer.auto-offset-reset=earliest"})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@EmbeddedKafka(topics = {"prices"}, bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@Transactional
class TradableKafkaPublisherTest {

    @Autowired
    ApplicationEventPublisher applicationEventPublisher;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    TradableRepository tradableRepository;

    BlockingDeque<ConsumerRecord<String, String>> records = new LinkedBlockingDeque<>();

    @KafkaListener(topics = "prices")
    void listen(ConsumerRecord<String, String> message) {
        records.add(message);
    }

    @Test
    void shouldPublishTradableAnnouncementAtRoundStart()
        throws InterruptedException, JsonProcessingException {
        publishGameStart();

        ConsumerRecord<String, String> record;
        do {
            record = records.pollFirst(5, TimeUnit.SECONDS);
        } while (!(new String(record.headers().lastHeader("type").value()).equals(
            "TradablePrices")));

        assertHeaders(record, Map.of("type", "TradablePrices"));

        var resourcePrices = objectMapper.readValue(record.value(),
            new TypeReference<List<Tradable>>() {
            });
        assertThat(resourcePrices)
            .hasSize(tradableRepository.findAll().size());
    }

    private void publishGameStart() {
        var gameStarted = new GameRoundStarted(UUID.randomUUID(), 1);
        applicationEventPublisher.publishEvent(gameStarted);
    }

    private void assertHeaders(ConsumerRecord<String, String> record, Map<String, String> expectedHeaders) {
        assertThat(record.headers().lastHeader("transactionId").value())
            .isNotNull();
        assertThat(record.headers().lastHeader("version").value())
            .isNotNull();
        assertThat(record.headers().lastHeader("timestamp").value())
            .isNotNull();
        assertThat(record.headers().lastHeader("eventId").value())
            .isNotNull();
        for(var expectedHeader : expectedHeaders.entrySet()) {
            assertThat(new String(record.headers().lastHeader(expectedHeader.getKey()).value()))
                .isEqualTo(expectedHeader.getValue());
        }
    }

}
