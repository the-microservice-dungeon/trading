package dungeon.trading.command;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import dungeon.trading.bank.BankAccount;
import dungeon.trading.bank.BankAccountRepository;
import dungeon.trading.player.Player;
import dungeon.trading.player.PlayerRepository;
import dungeon.trading.robot.Robot;
import dungeon.trading.robot.RobotRepository;
import dungeon.trading.tradables.TradableType;
import dungeon.trading.tradables.event.TradableBought;
import dungeon.trading.tradables.event.TradableSold;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import lombok.SneakyThrows;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest(properties = {"spring.kafka.consumer.auto-offset-reset=earliest"})
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@EmbeddedKafka(topics = {"trade-sell",
    "trade-buy"}, bootstrapServersProperty = "spring.kafka.bootstrap-servers")
@AutoConfigureMockMvc
class CommandControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private RobotRepository robotRepository;

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Nested
    class SellTest {

        BlockingDeque<ConsumerRecord<String, String>> sells = new LinkedBlockingDeque<>();

        @KafkaListener(topics = "trade-sell")
        void listen(ConsumerRecord<String, String> message) {
            sells.add(message);
        }

        @Test
        void shouldSellResources() {
            // Given
            var player = new Player(UUID.randomUUID(), "John Doe");
            var bankAccount = new BankAccount(player, BigDecimal.valueOf(0));
            var robot = new Robot(UUID.randomUUID(), player);

            robot.setInventory(Map.of(
                "COAL", 2,
                "IRON", 5,
                "GEM", 1,
                "GOLD", 0
            ));

            playerRepository.save(player);
            bankAccountRepository.save(bankAccount);
            robotRepository.save(robot);

            // When
            sendCommand("""
                [
                    {
                          "transactionId": "153aa3a6-576f-44b6-8df7-eb447ce9321e",
                          "playerId": "%s",
                          "payload": {
                            "commandType": "sell",
                            "robotId": "%s"
                          }
                    }
                ]
                """.formatted(player.getId(), robot.getId()));

            // Then
            var events = takeFromQueue(sells, "TradableSold", TradableSold.class, 3);
            assertThat(events)
                .hasSize(3)
                .satisfiesExactlyInAnyOrder(
                    event -> assertSell(event, player.getId(), robot.getId(), TradableType.RESOURCE, "COAL", 2),
                    event -> assertSell(event, player.getId(), robot.getId(), TradableType.RESOURCE, "IRON", 5),
                    event -> assertSell(event, player.getId(), robot.getId(), TradableType.RESOURCE, "GEM", 1)
                );

            var updatedBankAccount = bankAccountRepository.findBankAccountByPlayerId(player.getId())
                .get();
            assertThat(updatedBankAccount.getBalance())
                .isGreaterThan(BigDecimal.ONE);
        }
    }

    @Nested
    class BuyTests {

        BlockingDeque<ConsumerRecord<String, String>> buys = new LinkedBlockingDeque<>();

        @KafkaListener(topics = "trade-buy")
        void listen(ConsumerRecord<String, String> message) {
            buys.add(message);
        }

        // Robot is a special case because it does not require a robotId ID to be submitted
        // and it is central aspect of the game.
        @Nested
        class Robot {

            @Test
            void shouldBuyRobot() {
                // Given
                var player = new Player(UUID.randomUUID(), "John Doe");
                var bankAccount = new BankAccount(player, BigDecimal.valueOf(500));

                playerRepository.save(player);
                bankAccountRepository.save(bankAccount);

                // When
                sendCommand("""
                    [
                        {
                              "transactionId": "153aa3a6-576f-44b6-8df7-eb447ce9321e",
                              "playerId": "%s",
                              "payload": {
                                "commandType": "buy",
                                "itemName": "ROBOT",
                                "amount": 2
                              }
                        }
                    ]
                    """.formatted(player.getId()));

                // Then
                var event = getFromQueue(buys, "TradableBought", TradableBought.class);
                assertThat(event)
                    .satisfies(e -> assertBuy(e, player.getId(), null, TradableType.ITEM, "ROBOT", 2));
                assertThat(bankAccountRepository.findBankAccountByPlayerId(player.getId()))
                    .get()
                    .extracting(BankAccount::getBalance)
                    .satisfies(b -> assertThat(b).isEqualByComparingTo(BigDecimal.valueOf(300)));
            }
        }
    }

    @SneakyThrows
    private <T> ConsumerRecord<String, T> getFromQueue(
        BlockingDeque<ConsumerRecord<String, String>> queue, String typeHeader, Class<T> type) {
        Predicate<ConsumerRecord<String, String>> isType = r -> r != null && r.headers() != null
            && new String(r.headers().lastHeader("type").value()).equals(typeHeader);

        ConsumerRecord<String, String> record = await().atMost(5, TimeUnit.SECONDS)
            .until(queue::poll, isType);

        var recordValue = record.value();
        var mapped = objectMapper.readValue(recordValue, type);

        return new ConsumerRecord<>(record.topic(), record.partition(), record.offset(),
            record.key(), mapped);
    }

    private <T> List<ConsumerRecord<String, T>> takeFromQueue(
        BlockingDeque<ConsumerRecord<String, String>> queue, String typeHeader, Class<T> type, Integer take) {
        List<ConsumerRecord<String, T>> records = new ArrayList<>();
        while (records.size() < take) {
            records.add(getFromQueue(queue, typeHeader, type));
        }
        return records;
    }

    private void assertSell(ConsumerRecord<String, TradableSold> record, UUID playerId, UUID robotId, TradableType type,
        String name, Integer amount) {
        assertThat(record.value())
            .satisfies(e -> {
                assertThat(e.playerId()).isEqualTo(playerId);
                assertThat(e.robotId()).isEqualTo(robotId);
                assertThat(e.type()).isEqualTo(type);
                assertThat(e.name()).isEqualTo(name);
                assertThat(e.amount()).isEqualTo(amount);
                assertThat(e.pricePerUnit()).isPositive();
                assertThat(e.totalPrice()).isPositive();
            });
    }

    private void assertBuy(ConsumerRecord<String, TradableBought> record, UUID playerId, UUID robotId,
        TradableType type, String name, Integer amount) {
        assertThat(record.value())
            .satisfies(e -> {
                assertThat(e.playerId()).isEqualTo(playerId);
                assertThat(e.robotId()).isEqualTo(robotId);
                assertThat(e.type()).isEqualTo(type);
                assertThat(e.name()).isEqualTo(name);
                assertThat(e.amount()).isEqualTo(amount);
                assertThat(e.pricePerUnit()).isPositive();
                assertThat(e.totalPrice()).isPositive();
            });
    }

    void sendCommand(String content) {
        try {
            mockMvc.perform(
                    post("/commands")
                        .accept("application/json")
                        .contentType("application/json")
                        .content(content))
                .andExpect(status().isOk());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
